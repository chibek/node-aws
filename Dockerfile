FROM node:18.16.0-alpine AS build

WORKDIR /src 

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 5000

RUN npm run build

ENTRYPOINT ["node", "./dist/src/index.js"]